/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.db_store.TestConnection;

import java.sql.*;
import java.util.logging.Logger;


public class TestConnection {
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
      try {
         Class.forName("org.sqlite.JDBC");
         conn = DriverManager.getConnection("jdbc:sqlite:test.db" + dbPath);
         System.out.println("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error : JDBC is not exist");
        } catch (SQLException ex) {
            Logger.getLogger("Error : Database cannot connection");
        }
        
        /*
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.db"+ dbPath);
            System.out.println("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error : JDBC is not exist");
        } catch (SQLException ex) {
            Logger.getLogger("Error : Database cannot connection");
        }
        */
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error : cannot close database");
        }
    }
}
